//
// Created by mathias on 05.12.18.
//

#ifndef MODULENETWORK_NETWORKPROVIDER_HPP
#define MODULENETWORK_NETWORKPROVIDER_HPP

#include <unordered_map>
#include <list>
#include <Dolos_SDK/ModuleLoader/IModule.hpp>
#include <Dolos_SDK/Network/Network.hpp>
#include "INetworkProviderImpl.hpp"

# define NETWORK_MODULE_NAME "NetworkProvider"

namespace network
{
    class NetworkProvider : public INetworkProviderImpl, public dolos::IModule
    {
    public:
        NetworkProvider() = default;
        ~NetworkProvider() = default;

    // INetworkProvider implementation
    public:
        void addClient(std::shared_ptr<INetworkClientImpl> client) override;
        void setSerializer(dolos::network::ISerializer *serializer) override;
        void setConnectionHandler(dolos::network::IConnectionHandler *handler) override;
        void setPacketHandler(dolos::network::IPacketHandler *handler) override;
        bool open(unsigned int port, dolos::network::Protocol proto) override;
        void close(unsigned int port) override;

        // IModule implementation
    public:
        std::string getName() const override;
        void onEachFrame() noexcept override;

    private:
        void acceptNextClient(int socketFd);

    private:
        std::unordered_map<unsigned int, int> _sockets; // <port, socket>
        std::list<std::shared_ptr<INetworkClientImpl>> _clients;
        dolos::network::ISerializer *_serializer = nullptr;
        dolos::network::IConnectionHandler *_connectionHandler = nullptr;
        dolos::network::IPacketHandler *_packetHandler = nullptr;
    };
}

#endif //MODULENETWORK_NETWORKPROVIDER_HPP
