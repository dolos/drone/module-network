//
// Created by mathias on 05.12.18.
//

#ifndef MODULENETWORK_INETWORKPROVIDERIMPL_HPP
#define MODULENETWORK_INETWORKPROVIDERIMPL_HPP

#include <memory>
#include <Dolos_SDK/Network/INetworkProvider.hpp>
#include "INetworkClientImpl.hpp"

namespace network
{
    class INetworkProviderImpl : public dolos::network::INetworkProvider
    {
    public:
        INetworkProviderImpl() = default;
        virtual ~INetworkProviderImpl() = default;

    public:
        /**
         * Assume that the client is not registered
         * Used when the client just connected to the server
         * This function advise the NetworkManager of the client connection
         * @param client shared_ptr on the client
         */
        virtual void addClient(std::shared_ptr<INetworkClientImpl> client) = 0;
    };
}

#endif //MODULENETWORK_INETWORKPROVIDERIMPL_HPP
