//
// Created by mathias on 09.11.18.
//

#ifndef CORE_NETWORKCLIENT_HPP
#define CORE_NETWORKCLIENT_HPP

# define NETWORK_BUFF_SIZE 500
# define NETWORK_MSG_START "<Dolos_STR>"
# define NETWORK_MSG_END "</Dolos_STR>"

#include <sstream>
#include <queue>
#include <mutex>
#include <functional>
#include "INetworkClientImpl.hpp"

namespace network {
    class NetworkClient : public INetworkClientImpl, public std::enable_shared_from_this<INetworkClientImpl> {
    public:
        NetworkClient();

        ~NetworkClient() = default;

    public:
        void send(dolos::network::IPacket &packet) override;

        void close() override;

        bool isOpen() override;

        dolos::network::Protocol getProtocol() override;

        /**
         * Functions used by tests and NetworkProvider
         */
    public:
        void setDisconnectionHandler(const OnDisconnection &onDisconnection) override;
        void setPacketSerializer(dolos::network::ISerializer *serializer) override;
        void setPacketHandler(dolos::network::IPacketHandler *handler) override;
        void onConnectionClosed() override;
        std::string serializePacket(dolos::network::IPacket &p) override;
        std::shared_ptr<dolos::network::IPacket> deserialize(std::string rawData) override;

        void handleData(std::string data) override;

        /**
         * Defined in INetworkClientImpl, but used only by NetworkProvider and not by tests
         */
    public:
        void checkSelectValue(fd_set *rd_fds, fd_set *wr_fds) override;
        void addSocketToSelect(fd_set *rd, fd_set *wr, int *nfd) override;
        void setSocketFd(int fd) override;

    private:
        void doRead();
        void doWrite();

    private:
        // I/O
        int _endLen = 0;
        int _socketFd = -1;
        std::string _readBuffer;
        std::mutex _writeLock;
        std::vector<char> _writeBuffer;

        // Serialization / Deserialization
        dolos::network::ISerializer *_packetSerializer = nullptr;
        dolos::network::IPacketHandler *_packetHandler = nullptr;
        OnDisconnection _onDisconnect = nullptr;
    };
}

#endif //CORE_NETWORKCLIENT_HPP
