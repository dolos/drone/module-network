//
// Created by mathias on 05.12.18.
//

#ifndef MODULENETWORK_INETWORKCLIENTIMPL_HPP
#define MODULENETWORK_INETWORKCLIENTIMPL_HPP

#include <functional>
#include <Dolos_SDK/Network/INetworkClient.hpp>
#include <Dolos_SDK/Network/INetworkProvider.hpp>
#include <Dolos_SDK/Network/Network.hpp>

namespace network
{
    class INetworkClientImpl;
    using OnDisconnection = std::function<void(std::shared_ptr<INetworkClientImpl>)>;

    class INetworkClientImpl : public dolos::network::INetworkClient
    {
    public:
        INetworkClientImpl() = default;
        virtual ~INetworkClientImpl() = default;

        /**
         * These function are used by the NetworkProvider (and possibly the tests)
         */
    public:
        virtual void setPacketSerializer(dolos::network::ISerializer*) = 0;
        virtual void setPacketHandler(dolos::network::IPacketHandler*) = 0;
        virtual void setDisconnectionHandler(const OnDisconnection &onDisconnection) = 0;
        virtual void checkSelectValue(fd_set *rd_fds, fd_set *wr_fds) = 0;
        virtual void addSocketToSelect(fd_set *rd, fd_set *wr, int *nfd) = 0;
        virtual void setSocketFd(int fd) = 0;

    public:
        // These function are used by the NetworkClient and the Tests
        // These function should not be called by others objects than NetworkClient and Tests
        /**
         * Must call the disconnection handler
         */
        virtual void onConnectionClosed() = 0;
        virtual std::string serializePacket(dolos::network::IPacket &p) = 0;
        virtual std::shared_ptr<dolos::network::IPacket> deserialize(std::string rawData) = 0;
        virtual void handleData(std::string data) = 0;
    };
}

#endif //MODULENETWORK_INETWORKCLIENTIMPL_HPP
