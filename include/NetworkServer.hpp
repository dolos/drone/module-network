//
// Created by mathias on 06.11.18.
//

/*#ifndef CORE_NETWORKSERVER_HPP
#define CORE_NETWORKSERVER_HPP

#define RAPIDJSON_HAS_STDSTRING 1

#include <rapidjson/document.h>
#include <atomic>
#include <memory>
#include <Dolos_SDK/Network/INetworkServer.hpp>
#include <Dolos_SDK/ModuleLoader/IModule.hpp>
#include "NetworkClient.hpp"

# define NETWORK_MODULE_NAME "NetworkServer"

namespace server
{
    class NetworkServer : public dolos::network::INetworkServer, public dolos::IModule
    {
    public:
        NetworkServer() = default;
        NetworkServer(const NetworkServer&) = delete;
        NetworkServer &operator=(const NetworkServer&) = delete;
        ~NetworkServer() override;

    public:
        std::shared_ptr<dolos::network::ANetworkClient> getClient(const std::string &identifier) override;
        void setConnectionNotifier(ConnectionNotifier notifier) override;
        bool open(dolos::network::Protocol proto, int port) override;
        void close(dolos::network::Protocol proto) override;

        std::string getName() const override;

        void onEachFrame() noexcept override;

        void setPacketHandler(dolos::network::PacketReceivingHandler handler) override;

        void setDisconnectionNotifier(ConnectionNotifier notifier) override;

    private:
        void acceptNextClient(int socketFd);

    private:
        using Client = std::shared_ptr<NetworkClient>;
    private:
        dolos::network::PacketReceivingHandler  _onPacket = nullptr;
        ConnectionNotifier _onConnection = nullptr, _onDisconnection = nullptr;
        int _socketTcp = -1;
        int _socketUdp = -1;
        std::vector<Client> _clients;
    };

    class TestPacket : public dolos::network::INetworkObject
    {
    public:
        std::string getPacketName() override;

        std::string getSerializedData() override;
    };
}

#endif //CORE_NETWORKSERVER_HPP
*/