/*
** EPITECH PROJECT, 2018
** Core
** File  description:
** 
*/

#include "NetworkProvider.hpp"
#include <Dolos_SDK/ICore.hpp>

network::NetworkProvider networkProvider;

__exported FNC_MODULE_NAME
{
	return (NETWORK_MODULE_NAME);
};

__exported FNC_PRELOAD(core)
{
}

__exported FNC_SYS_LOAD(core, setter)
{
    setter->setNetworkServer(&networkProvider);
   return (&networkProvider);
}

__exported FNC_UNLOAD
{
};

__exported FNC_MODULE_TYPE
{
	return MODULE_SYSTEM;
};