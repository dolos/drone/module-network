//
// Created by mathias on 09.11.18.
//

#include <unistd.h>
#include <NetworkClient.hpp>
#include <Dolos_SDK/ICore.hpp>
#include <cstring>
#include <iostream>

# define RAPIDJSON_HAS_STDSTRING 1

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/writer.h>

using dolos::ICore;
using dolos::IThreadpool;

namespace network
{
    NetworkClient::NetworkClient() {
        _endLen = std::string(NETWORK_MSG_END).length();
    }

    void NetworkClient::send(dolos::network::IPacket &packet) {
        if (_socketFd == -1)
            return;
        std::string jsonStr = serializePacket(packet);
        // Add data to buffer
        std::unique_lock<std::mutex> l(_writeLock);
        auto inserter = std::back_inserter(_writeBuffer);
        std::copy(jsonStr.cbegin(), jsonStr.cend(), inserter);
    }

    void NetworkClient::close() {
        if (_socketFd == -1)
            return;
        // TODO: Close connection
        ::close(_socketFd);
        onConnectionClosed();
    }

    bool NetworkClient::isOpen() {
        return _socketFd != -1;
    }

    dolos::network::Protocol NetworkClient::getProtocol() {
        return dolos::network::TCP;
    }

    void NetworkClient::onConnectionClosed() {
        std::shared_ptr<INetworkClientImpl> ptr = shared_from_this();
        if (_onDisconnect != nullptr)
           _onDisconnect(ptr);
    }

    std::string NetworkClient::serializePacket(dolos::network::IPacket &packet) {
        if (_packetSerializer)
            _packetSerializer->onPacketSending(packet);
        std::string data = packet.serialize();
        std::string name = packet.getName();
        if (_packetSerializer)
            _packetSerializer->onDataSending(data);

        // Json
        rapidjson::Document d;
        d.SetObject();
        d.AddMember(rapidjson::Value("packet", d.GetAllocator()).Move(), rapidjson::StringRef(name), d.GetAllocator());
        d.AddMember(rapidjson::Value("data", d.GetAllocator()).Move(), rapidjson::StringRef(data), d.GetAllocator());

        rapidjson::StringBuffer  buffer;
        buffer.Clear();
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        d.Accept(writer);
        std::string jsonStr(NETWORK_MSG_START);
        jsonStr.append(buffer.GetString());
        jsonStr.append(NETWORK_MSG_END);
        return (jsonStr);
    }

    void NetworkClient::checkSelectValue(fd_set *rd_fds, fd_set *wr_fds) {
        if (FD_ISSET(_socketFd, rd_fds))
            doRead();
        else if (FD_ISSET(_socketFd, wr_fds))
            doWrite();
    }

    void NetworkClient::doRead() {
        char tmp[NETWORK_BUFF_SIZE];
        std::memset(tmp, 0, NETWORK_BUFF_SIZE);
        ssize_t size = read(_socketFd, tmp, NETWORK_BUFF_SIZE);
        if (size <= 0)
        {
            close();
            return;
        }
        auto inserter = std::back_inserter(_readBuffer);
        std::string tmpString(tmp);
        _readBuffer += tmpString;
        size_t endIdx = 0;
        // If a whole packet is contained in _readBuffer
        if ((endIdx = _readBuffer.find(NETWORK_MSG_END)) != std::string::npos)
        {
            // Extract packet and remove it from _readBuffer
            std::string data = _readBuffer.substr(_readBuffer.find(NETWORK_MSG_START), endIdx + _endLen);
            _readBuffer = _readBuffer.substr(endIdx + _endLen, _readBuffer.length());
            // Schedule deserialization on next server frame to avoid locking process too much
            dolos::getCore().getThreadPool().executeOnMainThread([this, data](){
               handleData(data);
            });
        }
    }

    void NetworkClient::doWrite() {
        std::unique_lock<std::mutex> l(_writeLock);
        if (_writeBuffer.empty())
            return;
        ssize_t size = write(_socketFd, &(_writeBuffer[0]), _writeBuffer.size());
        if (size == -1)
            return;
        _writeBuffer.erase(_writeBuffer.begin(), _writeBuffer.begin() + size);
    }

    void NetworkClient::addSocketToSelect(fd_set *rd, fd_set *wr, int *nfd) {
        FD_SET(_socketFd, rd);
        FD_SET(_socketFd, wr);
        if (*nfd <= _socketFd)
            *nfd = _socketFd;
    }

    void NetworkClient::setSocketFd(int fd) {
        _socketFd = fd;
    }

    void NetworkClient::setDisconnectionHandler(const OnDisconnection &onDisconnection) {
        _onDisconnect = onDisconnection;
    }

    void NetworkClient::setPacketSerializer(dolos::network::ISerializer *serializer) {
        _packetSerializer = serializer;
    }

    void NetworkClient::setPacketHandler(dolos::network::IPacketHandler *handler) {
        _packetHandler = handler;
    }

    std::shared_ptr<dolos::network::IPacket> NetworkClient::deserialize(std::string rawData) {
        rawData = rawData.substr(std::string(NETWORK_MSG_START).length(), rawData.length() - (std::string(NETWORK_MSG_START).length() + std::string(NETWORK_MSG_END).length()));
        if (rawData[rawData.length() - 1] == '<')
            rawData[rawData.length() - 1] = '\0';

        rapidjson::Document d;
        d.Parse(rawData);
        std::string packetName = d["packet"].GetString();
        std::string data = d["data"].GetString();
        if (_packetSerializer)
            _packetSerializer->onDataReceived(data); // Deserialize packet data;
        auto packet = _packetHandler->instantiate(packetName, data);
        if (_packetSerializer)
            _packetSerializer->onPacketReceiving(*packet);
        return (packet);
    }

    void NetworkClient::handleData(std::string data) {
        auto packet = deserialize(data);
        _packetHandler->onPacketReceived(packet, shared_from_this());
    }
}
