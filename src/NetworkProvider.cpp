//
// Created by mathias on 05.12.18.
//

#include <cstdio>
#include <cerrno>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include <algorithm>
#include <iostream>
#include <Dolos_SDK/ICore.hpp>
#include "NetworkProvider.hpp"
#include "NetworkClient.hpp"

# define PENDING_CON_ALLOWED 10

void network::NetworkProvider::addClient(std::shared_ptr<network::INetworkClientImpl> client) {
    _clients.push_back(client);
    client->setDisconnectionHandler([this](std::shared_ptr<INetworkClientImpl> cli){
        dolos::getCore().getThreadPool().executeOnMainThread([this, cli](){
            _clients.remove(cli);
            if (_connectionHandler)
                _connectionHandler->onDisconnection(cli);
        });
    });
    if (_connectionHandler)
        _connectionHandler->onConnection(client);
}

void network::NetworkProvider::setSerializer(dolos::network::ISerializer *serializer) {
    _serializer = serializer;
}

void network::NetworkProvider::setConnectionHandler(dolos::network::IConnectionHandler *handler) {
    _connectionHandler = handler;
}

void network::NetworkProvider::setPacketHandler(dolos::network::IPacketHandler *handler) {
    _packetHandler = handler;
}

bool network::NetworkProvider::open(unsigned int port, dolos::network::Protocol proto) {
    if (_sockets.find(port) != _sockets.end())
        return (false);

    // Opening socket
    int sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        return (false);

    // Socket binding
    sockaddr_in my_addr = {};
    std::memset(&my_addr, 0, sizeof(my_addr));
    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(port);
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (::bind(sockfd, (sockaddr*) &my_addr, sizeof(my_addr)) == -1) {
        ::close(sockfd);
        return (false);
    }

    // Start listening for connections
    if (listen(sockfd, PENDING_CON_ALLOWED) == -1)
    {
        ::close(sockfd);
        return (false);
    }

    // Store socket
    _sockets.emplace(port, sockfd);
    return true;
}

void network::NetworkProvider::close(unsigned int port) {
    auto it = _sockets.find(port);
    if (it == _sockets.end())
        return;
    ::close(it->second);
    _sockets.erase(port);
}

std::string network::NetworkProvider::getName() const {
    return NETWORK_MODULE_NAME;
}

void network::NetworkProvider::onEachFrame() noexcept {
    if (_sockets.empty() && _clients.empty())
        return;
    timeval tv;
    fd_set rd_fds, wr_fds;
    int nfd = 0;

    FD_ZERO(&rd_fds);
    FD_ZERO(&wr_fds);

    for (auto &p : _sockets)
    {
        FD_SET(p.second, &rd_fds);
        if (nfd <= p.second)
            nfd = p.second + 1;
    }
    for (auto &cli : _clients)
        cli->addSocketToSelect(&rd_fds, &wr_fds, &nfd);
    tv.tv_sec = 0;
    tv.tv_usec = 5;
    int ret = select(nfd + 1, &rd_fds, &wr_fds, nullptr, &tv);
    if (ret == -1) {
        std::cerr << "Select error" << std::endl; // TODO: Handle error
        std::perror(std::strerror(errno));
    }
    else if (ret)
    {
        for (auto &p : _sockets)
        {
            if (FD_ISSET(p.second, &rd_fds))
                acceptNextClient(p.second);
        }
        for (auto &cli : _clients)
            cli->checkSelectValue(&rd_fds, &wr_fds);
    }
}

void network::NetworkProvider::acceptNextClient(int socketFd) {
    struct sockaddr_in client_addr = {};
    socklen_t client_len = 0;

    std::memset(&client_addr, 0, sizeof(client_addr));
    int client_fd = ::accept(socketFd, (struct sockaddr*) &client_addr, &client_len);
    std::shared_ptr<INetworkClientImpl> client = std::make_shared<NetworkClient>();
    client->setSocketFd(client_fd);
    client->setPacketHandler(_packetHandler);
    addClient(client);
}