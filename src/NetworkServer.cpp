//
// Created by mathias on 06.11.18.
//
/*
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <iostream>
#include <Dolos_SDK/ICore.hpp>
#include <NetworkServer.hpp>
#include <rapidjson/filewritestream.h>
#include <rapidjson/writer.h>

# define PENDING_CON_ALLOWED 10

using rapidjson::Document;
using rapidjson::Value;
using dolos::ICore;
using dolos::network::INetworkServer;
using dolos::network::ANetworkClient;
using dolos::network::Protocol;

namespace server
{
    NetworkServer::~NetworkServer()
    {
        close(dolos::network::TCP);
        close(dolos::network::UDP);
    }

    std::shared_ptr<ANetworkClient> NetworkServer::getClient(const std::string &identifier) {
        return std::shared_ptr<ANetworkClient>();
    }

    void NetworkServer::setConnectionNotifier(INetworkServer::ConnectionNotifier notifier) {
        _onConnection = notifier;
    }

    void NetworkServer::setDisconnectionNotifier(INetworkServer::ConnectionNotifier notifier) {
        _onDisconnection = notifier;
    }

    bool NetworkServer::open(Protocol proto, int port) {
        addrinfo hints, *localhost, *iter;
        int sockfd = -1;

        // Find a local address where we can bind
        std::memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        if (proto == Protocol::TCP)
            hints.ai_socktype = SOCK_STREAM;
        else
            hints.ai_socktype = SOCK_DGRAM;
        hints.ai_flags = AI_PASSIVE; // Use localhost
        if (getaddrinfo(nullptr, std::to_string(port).c_str(), &hints, &localhost)!= 0)
            return (false);

        // Iter over available addresses to bind
        for (iter = localhost; iter != nullptr; iter = iter->ai_next)
        {
            if ((sockfd = socket(iter->ai_family, iter->ai_socktype, iter->ai_protocol)) == -1)
                continue;
            if (bind(sockfd, iter->ai_addr, iter->ai_addrlen) == -1)
            {
                ::close(sockfd);
                continue;
            }
            break;
        }
        freeaddrinfo(localhost);

        if (iter == nullptr)
            return (false);
        if (listen(sockfd, PENDING_CON_ALLOWED) == -1) {
            ::close(sockfd);
            return (false);
        }
        if (proto == Protocol::TCP)
            _socketTcp = sockfd;
        else
            _socketUdp = sockfd;
        return true;
    }

    void NetworkServer::close(Protocol proto) {
        if (proto == Protocol::TCP && _socketTcp != -1) {
            ::close(_socketTcp);
            _socketTcp = -1;
        }
        else if (proto == Protocol::UDP && _socketUdp != -1){
            ::close(_socketUdp);
            _socketUdp = -1;
        }
    }

    std::string NetworkServer::getName() const {
        return NETWORK_MODULE_NAME;
    }

    void NetworkServer::onEachFrame() noexcept {
        if (_socketTcp == -1 && _socketUdp == -1)
            return;

        struct timeval tv;
        fd_set rd_fds, wr_fds;
        int nfd = 0;

        FD_ZERO(&rd_fds);
        FD_ZERO(&wr_fds);
        if (_socketTcp != -1) {
            FD_SET(_socketTcp, &rd_fds);
            nfd = _socketTcp + 1;
        }
        if (_socketUdp != -1) {
            FD_SET(_socketUdp, &rd_fds);
            nfd = (_socketTcp > _socketUdp) ? _socketTcp + 1 : _socketUdp + 1;
        }
        for (auto &cli : _clients)
            cli->addSocketToSelect(&rd_fds, &wr_fds, &nfd);
        tv.tv_sec = 0;
        tv.tv_usec = 5;
        int ret = select(nfd + 1, &rd_fds, &wr_fds, nullptr, &tv);
        if (ret == -1)
            std::cerr << "Select error" << std::endl; // TODO: Handle error
        else if (ret)
        {
            if (_socketTcp != -1 && FD_ISSET(_socketTcp, &rd_fds))
                acceptNextClient(_socketTcp);
            if (_socketUdp != -1 && FD_ISSET(_socketUdp, &rd_fds))
                acceptNextClient(_socketUdp);
            for (auto &cli : _clients)
                cli->checkSelectValue(&rd_fds, &wr_fds);
        }
    }

    void NetworkServer::acceptNextClient(int socketFd) {
        struct sockaddr_in client_addr = {};
        socklen_t client_len = 0;

        std::memset(&client_addr, 0, sizeof(client_addr));
        int client_fd = ::accept(socketFd, (struct sockaddr*) &client_addr, &client_len);
        Client client = std::make_shared<NetworkClient>(_onPacket, client_fd);
        _clients.push_back(client);
        client->setOnDisconnectionHandler([this](Client c){
            auto it = std::find(_clients.begin(), _clients.end(), c);
            if (it != _clients.end())
                _clients.erase(it);
            if (_onDisconnection)
                _onDisconnection(c);
        });
        if (_onConnection != nullptr)
            _onConnection(client);
        dolos::network::Packet p;
        p.proto = dolos::network::TCP;
        p.networkObject = std::make_unique<TestPacket>();
        client->send(p);
    }

    void NetworkServer::setPacketHandler(dolos::network::PacketReceivingHandler handler) {
        _onPacket = handler;
    }

    std::string TestPacket::getPacketName() {
        return "Test_Packet";
    }

    std::string TestPacket::getSerializedData() {
        return "Serialized data";
    }
}*/