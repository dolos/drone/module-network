//
// Created by mathias on 05.12.18.
//

#include <gtest/gtest.h>
#include "NetworkTests.hpp"
#include "NetworkProvider.hpp"

using dolos::network::INetworkClient;
using network::INetworkClientImpl;
using network::NetworkProvider;
using testing::_;
using testing::InSequence;

/**
 * Test if the onConnection function from the ConnectionHandler is called when a client connect
 */
TEST(Network_Provider, ConnectionHandler)
{
    MockNetworkManager nm;
    std::shared_ptr<INetworkClient> client = std::make_shared<MockNetworkClient>();
    NetworkProvider provider;

    // SETUP ASSERTION

    EXPECT_CALL(nm, onConnection(client));
    EXPECT_CALL(*(std::dynamic_pointer_cast<MockNetworkClient>(client)), setDisconnectionHandler(_));

    provider.setConnectionHandler(&nm);
    provider.addClient(std::dynamic_pointer_cast<INetworkClientImpl>(client));
}

TEST(Network_Provider, Port_Opening)
{
    NetworkProvider provider;

    ASSERT_TRUE(provider.open(2709, dolos::network::TCP));
    ASSERT_FALSE(provider.open(2709, dolos::network::TCP));
    ASSERT_TRUE(provider.open(2710, dolos::network::TCP));
    provider.close(2709);
    provider.close(2710);
    provider.close(3909);
}