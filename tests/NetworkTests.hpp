//
// Created by mathias on 05.12.18.
//

#ifndef MODULENETWORK_NETWORKTESTS_HPP
#define MODULENETWORK_NETWORKTESTS_HPP

#include <gmock/gmock.h>
#include "INetworkProviderImpl.hpp"
#include <Dolos_SDK/ICore.hpp>
#include <Dolos_SDK/Network/Network.hpp>

# define VIRTUAL_PACKET "Virtual_Packet"

class MockNetworkClient : public network::INetworkClientImpl
{
public:
    MOCK_METHOD1(send, void(dolos::network::IPacket&));
    MOCK_METHOD0(close, void());
    MOCK_METHOD0(isOpen, bool());
    MOCK_METHOD0(getProtocol, dolos::network::Protocol());
    MOCK_METHOD0(onConnectionClosed, void());
    MOCK_METHOD1(setPacketSerializer, void(dolos::network::ISerializer*));
    MOCK_METHOD1(setPacketHandler, void (dolos::network::IPacketHandler*));
    MOCK_METHOD1(serializePacket, std::string(dolos::network::IPacket&));
    MOCK_METHOD1(deserialize, std::shared_ptr<dolos::network::IPacket>(std::string));
    MOCK_METHOD1(setDisconnectionHandler, void(const network::OnDisconnection&));
    MOCK_METHOD2(checkSelectValue, void(fd_set*, fd_set*));
    MOCK_METHOD3(addSocketToSelect, void(fd_set*, fd_set*, int*));
    MOCK_METHOD1(setSocketFd, void(int));
    MOCK_METHOD1(handleData, void(std::string));
};

/**
 * Thread pool mock
 * Used in DisconnectionHandler (only executeOnMainThread function)
 */
class MockThreadPool : public dolos::IThreadpool
{
public:
    MOCK_METHOD2(push, void(const Runnable &runnable, const Runnable &onOperationComplete));
    MOCK_METHOD2(pushDedicated, void(const Runnable &runnable, const Runnable &onOperationComplete));
    void executeOnMainThread(const Runnable &runnable) override {
        runnable();
    };
};

/**
 * Core mock
 * Core is used in DisconectionHandler to get the thread pool
 */
class MockCore : public dolos::ICore
{
private:
    MockThreadPool _pool;
public:
    MOCK_METHOD0(getModuleLoader, dolos::IModuleLoader&());
    MOCK_METHOD0(getRessourcesManager, dolos::IRessourcesManager&());
    MOCK_METHOD0(getWebService, dolos::API::IWebService&());
    dolos::IThreadpool &getThreadPool() override
    {
        return _pool;
    };
    MOCK_METHOD0(getLogger, dolos::Ilogger&());
    MOCK_METHOD0(getNetworkManager, dolos::network::INetworkManager&());
};

class MockNetworkManager : public dolos::network::IPacketHandler, public dolos::network::IConnectionHandler, public dolos::network::ISerializer
{
public:
  MOCK_METHOD2(onPacketReceived, void(std::shared_ptr<dolos::network::IPacket>, std::shared_ptr<dolos::network::INetworkClient>));
    MOCK_METHOD2(instantiate, std::shared_ptr<dolos::network::IPacket>(const std::string&, const std::string&));
    MOCK_METHOD1(onPacketSending, void(dolos::network::IPacket&));
    MOCK_METHOD1(onPacketReceiving, void(dolos::network::IPacket&));
    MOCK_METHOD1(onDataSending, void(std::string &));
    MOCK_METHOD1(onDataReceived, void(std::string&));
    MOCK_METHOD1(onConnection, void(std::shared_ptr<dolos::network::INetworkClient>));
    MOCK_METHOD1(onDisconnection, void(std::shared_ptr<dolos::network::INetworkClient>));
};

class VirtualPacket : public dolos::network::IPacket
{
public:
    VirtualPacket() : _data("SerializedData") {}
    VirtualPacket(const std::string &s) : _data(s) {}
public:
    std::string getName() const override {
        return VIRTUAL_PACKET;
    }

    std::string serialize() const override {
        return _data;
    }

    std::string _data;
};

#endif //MODULENETWORK_NETWORKTESTS_HPP
