//
// Created by mathias on 05.12.18.
//

#include <gtest/gtest.h>
#include "NetworkTests.hpp"
#include "NetworkClient.hpp"
#include "NetworkProvider.hpp"

using network::NetworkClient;
using network::NetworkProvider;
using testing::_;
using testing::InSequence;
using testing::Return;

TEST(Network_Client, DisconnectionHandler)
{
    MockCore core;
    std::shared_ptr<NetworkClient> client = std::make_shared<NetworkClient>();
    NetworkProvider provider;
    MockNetworkManager nm;
    dolos::setCore(&core);

    EXPECT_CALL(nm, onDisconnection(_)).Times(1);

    provider.setConnectionHandler(&nm);
    provider.addClient(client);
    client->onConnectionClosed();
    dolos::setCore(nullptr);
}

TEST(Network_Client, Serialization)
{
    std::shared_ptr<NetworkClient> client = std::make_shared<NetworkClient>();
    MockNetworkManager nm;
    VirtualPacket packet;
    // Expected serialized data for the virutal packet
    std::string expect = "<Dolos_STR>{\"packet\":\"Virtual_Packet\",\"data\":\"SerializedData\"}</Dolos_STR>";

    {
        InSequence dummy;
        EXPECT_CALL(nm, onPacketSending(_));
        EXPECT_CALL(nm, onDataSending(_));
    }

    client->setPacketSerializer(&nm);
    std::string res = client->serializePacket(packet);
    ASSERT_EQ(expect, res);
}

TEST(Network_Client, Deserialization)
{
    std::shared_ptr<NetworkClient> client = std::make_shared<NetworkClient>();
    MockNetworkManager nm;
    std::string data = "<Dolos_STR>{\"packet\":\"Virtual_Packet\",\"data\":\"Virtual_Packet\"}</Dolos_STR>";

    {
        InSequence dummy;
        EXPECT_CALL(nm, onDataReceived(_));
        EXPECT_CALL(nm, instantiate(VIRTUAL_PACKET, "Virtual_Packet")).WillRepeatedly(Return(std::make_shared<VirtualPacket>("Virtual_Packet")));
        EXPECT_CALL(nm, onPacketReceiving(_));
    }

    client->setPacketSerializer(&nm);
    client->setPacketHandler(&nm);
    auto packet = client->deserialize(data);
    ASSERT_FALSE(packet == nullptr);
    EXPECT_EQ(packet->getName(), VIRTUAL_PACKET);
    EXPECT_EQ(packet->serialize(), "Virtual_Packet");
}

TEST(Network_Client, PacketHandling)
{
    std::shared_ptr<NetworkClient> client = std::make_shared<NetworkClient>();
    MockNetworkManager nm;
    std::string data = "<Dolos_STR>{\"packet\":\"Virtual_Packet\",\"data\":\"Virtual_Packet\"}</Dolos_STR>";

    {
        InSequence dummy;
        EXPECT_CALL(nm, instantiate(VIRTUAL_PACKET, "Virtual_Packet")).WillRepeatedly(Return(std::make_shared<VirtualPacket>("Virtual_Packet")));
        EXPECT_CALL(nm, onPacketReceived(_, _));
    }

    client->setPacketSerializer(&nm);
    client->setPacketHandler(&nm);
    client->handleData(data);
}
